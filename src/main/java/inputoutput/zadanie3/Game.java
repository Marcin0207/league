package inputoutput.zadanie3;

public class Game {

    private String teamA, teamB;
    private int scoreA, scoreB;



    public Game (String teamA, int scoreA, int scoreB, String teamB){
        this.scoreA = scoreA;
        this.scoreB = scoreB;
        this.teamA = teamA;
        this.teamB = teamB;
    }

    public Game() {

    }

    @Override
    public String toString() {
        return  "teamA: " + teamA +
                ", teamB: " + teamB +
                ", scoreA=" + scoreA +
                ", scoreB=" + scoreB;
    }

}
