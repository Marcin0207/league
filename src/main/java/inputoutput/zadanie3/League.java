package inputoutput.zadanie3;

import java.util.ArrayList;
import java.util.List;

public class League {

   List<Game> gameHistory;

    public League() {
        gameHistory = new ArrayList<>();
    }
    public League(List<Game> gameHistory){
        this.gameHistory = gameHistory;
    }
    public List<Game> getGameHistory() {
        return gameHistory;
    }
}
