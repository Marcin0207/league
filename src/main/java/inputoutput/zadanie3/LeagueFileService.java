package inputoutput.zadanie3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class LeagueFileService {

    public void save(League league) {

        List<Game> matchlist = league.getGameHistory();
        Path path = Paths.get("liga.txt");
        try {
            BufferedWriter bufferedWriter = Files.newBufferedWriter(path);
            for (Game match: matchlist) {
                bufferedWriter.write(match.toString());
                bufferedWriter.newLine();


            }
            bufferedWriter.close();
        }catch (IOException e){
            System.out.println("błąd ścieżki");
        }
    }

}
