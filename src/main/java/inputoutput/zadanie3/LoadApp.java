package inputoutput.zadanie3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadApp {

    public static void main(String[] args) {

        List<Game> games = new ArrayList<>();
        League league;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("liga.txt"));
            String strCurrentLine;
            while ((strCurrentLine = bufferedReader.readLine()) != null) {

                String teamA = "";
                String teamB = "";
                int scoreA = 0;
                int scoreB = 0;
                String[] data = strCurrentLine.split(",");
                for (int i = 0; i < data.length; i++) {
                    if (data[i].contains("teamA")) {
                        teamA = data[i].substring(7);

                    }
                    if (data[i].contains("teamB")) {
                        teamB = data[i].substring(8);

                    }
                    if (data[i].contains("scoreA")) {
                        scoreA = Integer.parseInt(data[i].substring(8));

                    }
                    if (data[i].contains("scoreB")) {
                        scoreB = Integer.parseInt(data[i].substring(8));
                    }
                }
                games.add(new Game(teamA, scoreA, scoreB, teamB));

            }
            league = new League(games);
            for (Game game:league.getGameHistory()){
                System.out.println(game);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
