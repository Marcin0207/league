package inputoutput.zadanie3;

import java.util.Arrays;
import java.util.List;

public class SaveApp {

    public static void main(String[] args) {

        List<Game> games = Arrays.asList( new Game("RedSucs", 3,2,"Legia"),
                                        new Game("TurTurek",5,0,"Legia"),
                                                new Game("widzew",0,3,"Lech"));
        League league = new League(games);
        LeagueFileService ls = new LeagueFileService();
        ls.save(league);
    }

}
